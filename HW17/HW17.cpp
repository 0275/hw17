﻿#include <iostream>
#include <cmath>;
using namespace std;

class Person
{
private:
    string name, lastname;

public:

    Person() //конструктор по умолчанию
    {
        name = "Vanya";
        lastname = "Ivanov";
    }

    Person(string _name, string _lastname) //конструктор с параметрами
    {
        name = _name;
        lastname = _lastname;

    }


    void print()
    {
        cout << name << " " << lastname << endl;

    }

};

class Vector 
{

private:
    double x;
    double y;
    double z;

public:

    Vector()
    {
        x = 0;
        y = 0;
        z = 0;
    }


    Vector(double _x, double _y, double _z)
    {
        x = _x;
        y = _y;
        z = _z;
        
    }

    void show()
    {
        cout << " x = " << x << " y = " << y << " z = " << z << endl;

    }

    
    double getLength() 
    {
        
        return sqrt(x * x + y * y + z * z);
    }

};

int main()
{
    
    Person n;
    n.print();
    Person n2 ("Grigorii" , "Pupkov");
    n2.print();
    Vector a(1, 2, 3);
    a.show();
    cout << "Length is " << a.getLength();

}